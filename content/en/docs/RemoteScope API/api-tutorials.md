---
title: "API tutorials"
date: 2020-03-27
weight: 5
description: >
  API tutorials demonstrate how to use the API methods to satisfy use cases
---

{{% pageinfo %}}
Basic, advanced use cases demonstrated as tutorials.
{{% /pageinfo %}}


## Remote telescope booking

Description how to automate remote telescope booking


## Remote telescope operation

For example, point the telescope at one point in the sky for the first half of your observation time, then point it to another part of the sky.

## (NEW!) Remote astrophotography

Starting March 2020, we have equipped a number of telescopes with powerful cameras, so you can now perform astrophotography tasks using RemoteScope.


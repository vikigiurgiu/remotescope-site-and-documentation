---
title: "Authentication"
date: 2020-03-27
weight: 2
description: >
  Instructions how to authenticate to the RemoteScope API
---

{{% pageinfo %}}
RemoteScope uses [Oauth2](http://oauthbible.com/#oauth-2-three-legged) for authentication to the API. Read this page to learn how to get a user token and authenticate.
{{% /pageinfo %}}

Authentication information to follow here. We're in this for the long run.



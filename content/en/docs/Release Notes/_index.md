
---
title: "Release Notes"
linkTitle: "Release Notes"
weight: 7
date: 2020-03-27
description: >
  What's new in the RemoteScope world
---

{{% pageinfo %}}
Browse our release notes to find out what the team has been working on.
{{% /pageinfo %}}

## March 2020

### New features

| Feature            | Description    |
|-------------------|-----------------|
| Astrophotography   | Starting March 2020, we have equipped a number of telescopes with powerful cameras, so you can now perform astrophotography tasks using RemoteScope.        |
| Guided tours       | You can now book your telescope time with a guide who will explain things to you and point out cool things in the night sky.  |

### Fixes and improvements

* Fixed an issue where a customer saw a flying saucer. We kindly asked the visitors to not disturb our viewers' telescope time. 

## February 2020

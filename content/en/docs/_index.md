
---
title: "RemoteScope Documentation"
linkTitle: "Documentation"
weight: 20
menu:
  main:
    weight: 20
---

{{% pageinfo %}}
Use the documentation in this section to get started using RemoteScope and to integrate with RemoteScope for more advanced use cases. 
{{% /pageinfo %}}


This section covers information from Getting Started for basic use cases, information about our telescope technology in the Reference section, tutorials, and advanced technical documentation to use our telescopes in an automated manner. More advanced use cases include booking telescope hours in an automated manner, remote operation, and even remote astrophotography.  

For information about the monthly RemoteScope events, see our [Blog](/blog/) section.





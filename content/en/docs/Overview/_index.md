---
title: "Overview"
linkTitle: "Overview"
weight: 1
description: >
  General information about RemoteScope
---

{{% pageinfo %}}
RemoteScope overview documentation
{{% /pageinfo %}}


## What is RemoteScope?

We have an array of 40 telescopes in upstate New York that you can operate remotely to explore the night sky.

## Why do I want it?

For beginners, this could be your entry into astronomy, peaking at the night sky from the comfort of your home and assisted by our professionals.

For advanced users, our API allows you to integrate with our telescopes and automate your workflows. How far you can go and what you can do is up to you and your creativity.

* **What is it good for?**: Will detail some use cases here.

* **What is it not good for?**: People that don't want awesome things. Not you, but we all have that friend...

* **What is it *not yet* good for?**: In spring 2021, we'll launch out RemoteScope Orbit, the first telescope in near-earth orbit that regular people can use. 

## Where should I go next?

Start with the following sections

* [Getting Started](/content/en/docs/Getting started/_index.md)
* [Authentication](content/en/docs/RemoteScope API/api-authentication.md): Authenticate to the API

